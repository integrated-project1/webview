# WebView microservice

The Webview microservice renders and manages all web pages with Jinja, attributing the routes for them. It is also in charge of caching some information.

## How to run the code (with docker)

..... is coming...
First build the docker image with:

```docker-compose build```

Then you can run the container with:

```docker-compose up```

## Templates

Jinja allows us to renders some Templates.

## Caching

Coming soon 

## Input from reviews

### Sprint 0

- /

### Sprint 1

- The web site has the potential to handle sensitive data (power consumption, users, ...).  Are there security policies scheduled in the whole development?  How do you manage rights/privileges on the web site?  How do you ensure secure connection between the backbone and sensors? -> __TODO__ : issue [#6](https://gitlab.com/integrated-project1/webview/-/issues/6) and issue [#2](https://gitlab.com/integrated-project1/webview/-/issues/21)

### Sprint 2

- Unit tests described in the report are quite basic.  Why not having developed an actual Unit test environment (using, e.g., PyUnit)? -> __TODO__ issue ?
- The overall look and feel of the website is very basic. Have a look at libraries like Bootstrap to easily improve its quality. ✅ in progress in issue [#4](https://gitlab.com/integrated-project1/webview/-/issues/4)
- We would like to see: processing and data separation, user error management, exception handling, logging, documentation inside and outside code, proper code styling, files and directories with meaningful names and sound  -> __GENERAL__ Check doc and coding style
- Also, answer to these questions: what if the customer wants to add new buildings in the system? ->  __TODO__ issue [#7](https://gitlab.com/integrated-project1/webview/-/issues/7)

### Sprint 3

- Response time for 250 users is less than for 100 users in some cases. (slide 25)  How can you explain that? -> __TODO__ Load test issue ?
- On the website, how can I get the power consumption for B28 alone? -> __TODO__ 
- Login as « admin » is quite unfriendly as it requires the user to refresh the page after the login succeeded (I first thought I messed up the login/pwd).  The user should be redirected automatically -> __TODO__ : issue [#6](https://gitlab.com/integrated-project1/webview/-/issues/6)
- Now that your solution is more elaborated, it is clear that you built an SPA. Unfortunately, you didn't really planned this, and did not used a dedicated SPA framework (such as Angular), which has a strong negative impact on the quality of the website. -> ✅ We do MPA
- Using your website, one can see for the production graph that every night there are periods with missing data. Obviously, during the night, the solar panels are not producing energy, and thus the data is not missing. This is the kind of elements that clearly shows that your solution is over simplistic regarding the problem you had to solve.  Having some data manipulation in your server, which is so far a "dumb" proxy, would have allowed you to solve this problem as well as the next.  If you display a period starting at night, there is no display of the "missing data" line, since your script fails at detecting the starting point of the "problematic" section.  Web browser freezes when exporting large amount of data. Data export also dumps a lot of logs in the console.  Map bugs every time after a data export.  You have multiple loading of the same JS libraries with different versions.  There is no state management inside the website (which would have been easy to achieve with SPA in mind).  -> ???
- The use of 2 DBs is not really justified. You have passwords hardcoded and duplicated inside your code.  While you worked on the structure of your code as asked, there is still a lot of room for improvement. Python code is still lacking many good practices regarding styling and documentation.  JS code is ok.  The fact that it takes 2 pages of texts to explain how to add a building should be enough for you to know this is not convenient. What about adding a command in admin section allowing to manage buidlings with a form?

### Final review

- Update dashboard in DATA with energy consumption or production. -> __TODO__ issue ?

### Resit review

- Given the fact you decided to put in place a REST API on your server, we expect you to build a web client that benefits from it while being properly structured. This means either building a SPA or building an intermediary web server generating pages. Either way, we expect you to use one of the many well-established frameworks available to you, instead of putting together a few scripts. ✅ We do MPA 
- The end result needs to be a working website with fully functional navigation, which includes URL navigation. Also, pages must be isolated in a way that using a page should never lead another one to fail. ✅ We do MPA and Jinja, -> verify if there is no interference
- Building management should be automated as much as possible and accessible through a dedicated form. A map can be used to draw the electric network. ->  __TODO__ issue [#7](https://gitlab.com/integrated-project1/webview/-/issues/7)
-Since you will increase the load of the server, it is important to make sure its performances remain acceptable. In order to achieve this, you need to implement various mechanisms such as caching. You will evaluate the performances of your system with and without these mechanisms -> __TODO__ issue ?