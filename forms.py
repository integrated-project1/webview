from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField
from wtforms.validators import DataRequired

#######################################################
#CSRF Protected forms used to control users management.#
#######################################################

class LoginForm(FlaskForm):

    """simple login form with username, password and submit button
    Be mindful that FlaskForm(parent class) embeds a csrf_token in the form 

    Attributes:
        password (PasswordField): form field to input password
        submit (SubmitField): form submit button
        username (StringField): form field to input username
    """
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):

    """Registration form
    Be mindful that FlaskForm embeds a csrf_token in the form
    
    Attributes:
        username (StringField): username field
        password (PasswordField): password field to give to the registered user
        modifySite (BooleanField): boolean field to tick if the user should be
                                   able to modify the website
        registerPeople (BooleanField): boolean field to tick if the user should
                                       be able to register and manage users
        accessData (BooleanField): boolean field to tick if the user should
                                   be able to see and export all data
        submit (SubmitField): submit button
        
    """
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    modifySite = BooleanField('Modify the website', default="checked")
    registerPeople = BooleanField('Register and manage users')
    accessData = BooleanField('Export and see all data', default="checked")
    submit = SubmitField('Register')


class ManageUsersForm(FlaskForm):
    """Manage Users form
    Attributes:
        username (SelectField): select field to choose which user to modify
        modifySite (BooleanField): boolean field to tick if the user should be
                                   able to modify the website
        registerPeople (BooleanField): boolean field to tick if the user should
                                       be able to register and manage users
        accessData (BooleanField): boolean field to tick if the user should
                                   be able to see and export all data
        delete (BooleanField): boolean field to tick if the user should be
                               deleted
        submit (SubmitField): submit button
    """
    username = SelectField('User', choices=[], validators=[DataRequired()])
    modifySite = BooleanField('Modify the website', default="checked")
    registerPeople = BooleanField('Register and manage users', default="checked")
    accessData = BooleanField('Export and see all data', default="checked")
    delete = BooleanField('Delete User')
    submit = SubmitField('Update User')

class PasswordModifyForm(FlaskForm):

    """form to let the user change his password
    Be mindful that the parent class FlaskForm embeds a csrf_token in the form
    Attributes:
        newPassword (PasswordField): password field to input new password
        submit (SubmitField): submit button
    """
    newPassword = PasswordField('New password', validators=[DataRequired()])
    submit = SubmitField('Modify password')