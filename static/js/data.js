/**
 * ELECTRICAL ENERGY AT A GLANCE
 *
 * Authors: Team 2:
 * Maxence CALIXTE, Saul ESCALONA, Adrien SCHOFFENIELS
 * Tanguy SPITS, Arian TAHIRAJ, Dominik ZIANS
 * 
 * This file contains classes and functions for drawing diagrams
 */



/*****************************************************************************
 Constants
*****************************************************************************/

// Time interval from which the api should return
// data with a timestep of 15 minutes rather than
// 1 minute (in hours)
const INTERVAL_TIMESTEP = 8;

// Datatypes units
const CURRENT_UNIT = 'A';
const VOLTAGE_UNIT = 'V';
const POWER_UNIT = 'kW';
const REACTIVE_POWER_UNIT = 'kvar';
const COS_PHI_UNIT = 'rad';

// Size of the markers on the diagrams
const MARKER_SIZE = 6;

// Number of decimals to show for the indicators
const DECIMALS = 0;

/*****************************************************************************
 The DataInfo class
*****************************************************************************/


/**
 * A class containing all information that is needed in order to request data
 *
 * @param {string} building - The building 
 * @param {string or array} datatype - The type(s) of data to show (e.g. voltage, current,...)
 * @param {Date or null} from - The first data point to show
 * @param {Date or null} to - The last data point to show
 */
function DataInfo(building, datatype, from, to) {
  this.building = building;
  this.datatype = datatype;
  this.from = from;
  this.to = to;
  this.isLive = false;
  // Set the timeStep if dates are given
  if (from) {
    if (this.to.diff(this.from, 'hours') >= INTERVAL_TIMESTEP) {
      this.timestep = 15;
    }
    else {
      this.timestep = 1;
    }
  }


  /**
   * Get the timestep needed regarding the time interval
   * @returns The timestep
   */
  this.setTimestep = function () {
    if (this.to.diff(this.from, 'hours') >= INTERVAL_TIMESTEP) {
      this.timestep = 15;
    }
    else {
      this.timestep = 1;
    }
  }


  /**
   * Shift the dates of a DataInfo by a given number of given units
   *
   * @param {number} shift - The number of unit to shift the date
   * @param {string} unit - The unit to shift the Date by ('minute', 'hour', 'day', 'week', 'month', 'year')
   * 
   * @returns A copy of the initial DataInfo but the dates are shifted by 'shift' units
   */
  this.shiftedDates = function (shift, unit) {
    return new DataInfo(
      this.building,
      this.datatype,
      this.from.add(shift, unit),
      this.to.add(shift, unit)
    );
  }


  /**
   * Updates the info to only request data since last date on the diagram
   * 
   */
  this.liveUpdate = function (fromDate) {
    this.from = moment(fromDate);
    this.to = moment();
    this.isLive = true;
  }


  /**
   * Get the unit of the datatype
   * @returns The unit of the datatype inside the info class
   */
  this.getUnit = function () {
    if (this.datatype.includes('power')) {
      if (this.datatype.includes('reactive')) {
        return REACTIVE_POWER_UNIT;
      }
      else {
        return POWER_UNIT;
      }
    }
    else if (this.datatype.includes('voltage')) {
      return VOLTAGE_UNIT;
    }
    else if (this.datatype.includes('current')) {
      return CURRENT_UNIT;
    }
    else if (this.datatype.includes('phi')) {
      return COS_PHI_UNIT;
    }
    // Unknown
    else return '/';
  }
}


/*****************************************************************************
 The Diagram class
*****************************************************************************/


/**
 * A class containing all information of a diagram
 *
 * @param {string} div_id - The id of the div where the diagram should be drawn in
 * @param {dict} layout  - A dictionary for the Plotly library, defines how the diagram looks like
 * @param {dict} options - A dictionary for the Plotly library, defines how the diagram looks like
 # @param {bool} complete - True if the diagram needs all details (such as indicators, ...)
 */
function Diagram(div_id, layout, options, complete) {
  this.div_id = div_id;
  this.layout = layout;
  this.options = options;
  this.complete = complete;

  // Handler that refreshes the diagram every minute
  this.live_interval = null;

  // Metadata and data of what the diagram is showing currently
  this.info = null;
  this.data = null;
  this.dates = null;

  // Metadata and data to compare from different times
  this.compare_date = null;


  /**
   * Set the metadata of the data that should be drawn
   * This method should be used for the manual mode
   * 
   * @param {DataInfo} info - All the information needed
   */
  this.setInfo = function (info) {
    this.info = info;
  }


  /**
   * Remove the metadata of the data that should be drawn
   */
  this.clearInfo = function () {
    this.info = null;
    this.data = null;
    this.dates = null;
  }


  /**
   * Set the metadata of the data to compare that should be drawn
   * 
   * @param {DataInfo} info - All the information needed
   */
  this.setCompareInfo = function (compare_date) {
    this.compare_date = compare_date;
  }


  /**
   * Remove the metadata of the data to compare that should be drawn
   */
  this.clearCompareInfo = function () {
    this.compare_date = null;
    this.compare_data = null;
  }


  /**
   * Set the metadata of the data that should be drawn
   * This method should be used for the live mode
   * The diagram will show the last 'shift' units since now
   * 
   * @param {DataInfo or null} info - All the information needed ('from' and 'to' will be ignored)
   *                                     If null, the old info will be kept
   * @param {number} shift - The number of units to show of the data
   * @param {string} unit - The unit to shift the data by ('minute', 'hour', 'day', 'week', 'month', 'year')
   */
  this.setLiveDisplay = function (info, shift, unit) {
    if(this.live_interval) {
      // if it was live before, stop the repetition
      clearInterval(this.live_interval);
      this.live_interval = null;
    }

    // save the info and add the appropriate dates to it
    if(info) {
      this.info = info;
    }
    else if(this.info == null) {
      throw "info is null but no info to keep";
    }
    this.info.to = moment();
    this.info.from = moment(this.info.to).subtract(shift, unit);
    this.info.setTimestep();
  }


  /**
   * Remove the metadata of the live-data that should be drawn
   */
  this.clearLiveDisplay = function () {
    this.pauseLiveDisplay();
    this.clearInfo();
  }


  /**
   * Pause the minutely update of the diagram
   */
  this.pauseLiveDisplay = function () {
    if(this.live_interval) {
      clearInterval(this.live_interval);
      this.live_interval = null;
    }
  }


  /**
   * Start/restart the minutely update of the diagram
   */
  this.startLiveDisplay = function () {
    // Show the first plot then update it every minute with one datapoint
    this.loadData(this.info)
    var this_object = this;
    // 'this' in JS is different from 'this' in Java, or 'self' in Python. If we would use
    // simply 'this' in the next line, it would not be interpreted as this Diagram, but
    // the object that calls the handler every minute, therefor we use 'this_object'
    this.live_interval = setInterval(function() {this_object.liveDisplay()}, 60000 * this_object.info.timestep);
  }


  /**
   * Update the information of the live diagram
   */
  this.liveDisplay = function () {
    if(this.info != null) {
      // update the info and request the data
      this.info.liveUpdate(this.dates[this.dates.length-1]);
      this.loadData(this.info);
    }
  }

  /**
   * Show the retrieved data in the diagram
   */
  this.updateView = function () {

    // If live, remove the last datapoint(s) and add the new one(s)
    // Culd have serveral datapoints if connection to API lost
    // at previous update(s), or if processing of a datapoint
    // took too much time and was not available. This
    // prevents to have gaps in the plots.
    if (this.info.isLive) {
      if (this.data.length == 0) {
        // Nothing to change if no data returned
        return
      }

      // Prevents deleting 2 datapoints when the first new datpoint is in
      // fact the last one of the plot
      var this_object = this;
      this.dates.forEach(function(item, i) {
        if (!moment(this_object.dates[0]).isAfter(moment(this_object.plotData[0].x[this_object.plotData[0].x.length-1]))) {
          this_object.dates.splice(0, 1);
          this_object.data.splice(0, 1);
          if (this_object.compare_data) {
            this_object.compare_data.splice(0, 1);
          }
        }
      });

      if (this.data.length == 0) {
        // Nothing to change if no new data reveived
        return
      }

      // Delete the oldes datapoints on the plot that will be replaced
      this.plotData[0].x.splice(0, this.dates.length);
      this.plotData[0].y.splice(0, this.dates.length);
      this.plotData[0].marker.color.splice(0, this.dates.length);

      // Add the new dates
      this.plotData[0].x.push(...this_object.dates.map(x => new Date(x)));

      // Replace null values by minimum value and set the color to red for this datapoint
      this.data.forEach(function(item, i) {
        if (item === null){
          this_object.plotData[0].y.push(MINIMUMS[this_object.info.building][this_object.info.datatype]);
          this_object.plotData[0].marker.color.marker.color.push('rgb(255,0,0)');
        }
        else {
          this_object.plotData[0].y.push(item);
          this_object.plotData[0].marker.color.push('rgb(31,119,180)');
        }
      });

      // Also update comparison data if needed
      if (this.compare_data) {
        this.plotData[1].y.splice(0, this.dates.length);
        this.plotData[1].marker.color.splice(0, this.dates.length);

        // Set the same dates as for the main data
        this.plotData[1].x = this.plotData[0].x;
        // Replace null values by minimum value and set the color to red for this datapoint
        this.compare_data.forEach(function(item, i) {
          if (item === null){
            this_object.plotData[1].y.push(MINIMUMS[this_object.info.building][this_object.info.datatype]);
            this_object.plotData[1].marker.color.marker.color.push('rgb(255,0,0)');
          }
          else {
            this_object.plotData[1].y.push(item);
            this_object.plotData[1].marker.color.push('rgb(239,119,13)');
          }
        }); 
      }

      // Is set to true in liveDisplay, when point is queried
      this.info.isLive= false; 
    }

    // If isLive not true, first time plotting specific data
    else {
 
      // Put all the data in a single list
      this.plotData = [];

      // First plot the main data
      if(this.data) {
        // Basic plot data
        var mainData = {
          x: this.dates.map(x => new Date(x)),
          y: [],
          name: 'Main data',
          fill: 'tozeroy',
          mode: 'lines+markers',
          marker: {
            color: [],
            size: MARKER_SIZE
          }
        };
        // Get minimum value for datatype
        var minValue = MINIMUMS[this.info.building][this.info.datatype]

        //Create the color array
        var colors = [];
        // Replace null values by minimum value and set the color to red for this datapoint
        this.data.forEach(function(item, i) {
          if (item === null){
            mainData.y.push(minValue);
            mainData.marker.color.push('rgb(255,0,0)');
          }
          else {
            mainData.y.push(item);
            mainData.marker.color.push('rgb(31,119,180)');
          }
        });
        this.plotData.push(mainData);
      }

      // Add the compare data if requested
      if(this.compare_data) {
        // Basic plot data (dates should be the same as for main data)
        var compareData = {
          x: mainData.x,
          y: [],
          name: 'Comparison data',
          fill: 'tonexty',
          mode: 'lines+markers',
          marker: {
            color: [],
            size: MARKER_SIZE
          }
        };

        //Create the color array
        var compare_colors = [];
        // Replace null values by minimum value and set the color to red for this datapoint
        this.compare_data.forEach(function(item, i) {
          if (item === null){
            compareData.y.push(minValue);
            compareData.marker.color.push('rgb(255,0,0)');
          }
          else {
            compareData.y.push(item);
            compareData.marker.color.push('rgb(239,119,13)');
          }
        });

        this.plotData.push(compareData);
      }
    }

    // plot everything in the list
    Plotly.newPlot(this.div_id, this.plotData, this.layout, this.options);
    if (this.complete && this.plotData[0]) {
      // Displays indicators if needed
      this.indicatorDisplay(this.plotData[0].y);
    }
  }

  /**
   * Shows the indicators corresponding to the requested data.
   */
  this.indicatorDisplay = function (data) {

    // Compute the sum of all the data 
    var total = data.reduce((a,b) => a + b, 0);

    // Display the energy only if the datatype is a power
    if (this.info.datatype.includes('power') && !this.info.datatype.includes('reactive')) {
      document.getElementById('energy').style.display = 'flex';
      document.getElementById('energyVal').innerHTML = (total/(60/this.info.timestep)).toFixed(DECIMALS);
    }
    else {
      document.getElementById('energy').style.display = 'none';
    }

    // Display the mean value of the data
    document.getElementById('mean').innerHTML = this.info.datatype + ' (' + this.info.getUnit() + '):';
    document.getElementById('meanVal').innerHTML = (total/this.data.length).toFixed(DECIMALS);

    // Display the max value of the data
    document.getElementById('max').innerHTML = this.info.datatype + ' (' + this.info.getUnit() + '):';
    document.getElementById('maxVal').innerHTML = (Math.max(...data)).toFixed(DECIMALS);

    // Display the min value of the data
    document.getElementById('min').innerHTML = this.info.datatype + ' (' + this.info.getUnit() + '):';
    document.getElementById('minVal').innerHTML = (Math.min(...data)).toFixed(DECIMALS);
  }

  /**
   * Load the data, then update the diagram
   */
  this.loadData = function () {
    var this_object = this;

    // Reset the error messages
    if (this.complete) {
      document.getElementById("limited").innerHTML = "";
    }
    document.getElementById("error").innerHTML = "";

    // Set the request options
    var opts = {
      'datatypes': this.info.datatype,
      'timestep': this.info.timestep
    };


    // Api sends data object (columns, index, data)
    data_api.getData(this.info.building, this.info.from.format(), this.info.to.format(), opts, (error, data, response) => {
      if (error) {
        // If response is undefined, the connection to the API failed
        if (typeof response === 'undefined') {
          document.getElementById("error").innerHTML = "<p class='alert alert-danger'><b>Connection error.</b> Try again later.</p>";
        }
        // Not-authorized response
        else if (response.statusCode == 403) {
          document.getElementById("limited").innerHTML = response.body;
        }
        // Unknown error
        else {
          console.error(error);
        }
      }
      else {
        this_object.dates = data.index;
        // Put the data in a unique array
        this_object.data = [].concat.apply([], data.data);
        // Draw the diagram if comparison data not needed
        if (!this.compare_date) {
          this_object.updateView();
        }
      }
    });

    // Request data for comparison if needed
    if (this.compare_date) {

      //Compute the time shift needed
      var shift = this.compare_date.diff(this.info.from, 'minutes');
      data_api.getData(this.info.building, this.info.from.add(shift, 'minutes').format(), this.info.to.add(shift, 'minutes').format(), opts, (error, data, response) => {
        if (error) {
          // If response is undefined, the connection to the API failed
          if (typeof response === 'undefined') {
            document.getElementById("error").innerHTML = "<p class='alert alert-danger'><b>Connection error.</b> Try again later.</p>";
          }
          // Not-authorized response
          else if (response.statusCode == 403) {
            document.getElementById("limited").innerHTML = response.body;
          }
          // Unknown error
          else {
            console.error(error);
          }
        }
        else {
          // Put the data in a unique array
          this_object.compare_data = [].concat.apply([], data.data);
          this_object.updateView();
          // Ignore the dates since we want to plot the comparison data on top of the main data
        }
      });
    }
  }
}
