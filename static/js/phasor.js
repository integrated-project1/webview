/**
 * ELECTRICAL ENERGY AT A GLANCE
 *
 * Authors: Team 2:
 * Maxence CALIXTE, Saul ESCALONA, Adrien SCHOFFENIELS
 * Tanguy SPITS, Arian TAHIRAJ, Dominik ZIANS
 * 
 * This file contains classes and functions for drawing the phasor diagram
 */


/*******************************************************************************
 Constants
*******************************************************************************/

// Column names as returned by the api (since phasor needs precise columns, can not
// have the column names generated by jinja or so)
const V12 = "voltage_12";
const V23 = "voltage_23";
const V31 = "voltage_31";
const I1 = "current_1";
const I2 = "current_2";
const I3 = "current_3";
const COS = "cos_phi";


// Values used by the phasor
const MAX_ADMISSIBLE_VOLTAGE = 12000;
const MEAN_V = 8660;


// Creation of the phasor
const PHASOR_DIAGRAM = new Phasor();
const PHASOR_PLAYER = new PhasorPlayer();


/*******************************************************************************
 The Phasor class
*******************************************************************************/


function Phasor() {  
  this.data = null;

  // Total number of data point and current time step
  this.state = 0;
  this.size = 0;

  // Mean around which fluctuations are amplified
  this.v_mean = 0;

  // Phasor's canvas and context
  this.canvas = null;
  this.ctx = null;

  // Get the state of the phasor
  this.getState = function() {
    return this.state;
  }

  // Set the state of the phasor
  this.setState = function(state) {
    this.state = state;
    document.getElementById("phasor_slider").value = state;
    this.updatePhasor();
  }

  // Get the size of the phasor
  this.getSize = function() {
    return this.size;
  }

  // Load data using the javascript client
  this.loadData = function(info) {
    var this_object = this;

    // Reset the error message
    document.getElementById("limited").innerHTML = "";
    document.getElementById("phasor_error").innerHTML = "";

    // Get the timestep needed
    if (info.to.diff(info.from, 'hours') > INTERVAL_TIMESTEP) {
      var timestep = 15;
    }
    else {
      var timestep = 1;
    }

    // Put the datatypes in the right format
    opts = {
      'datatypes': [V12, V23, V31, I1, I2, I3, COS].join(' '),
      'timestep': timestep
    };

    // Use javascript client to request data from the API
    data_api.getData(info.building, info.from.format(), info.to.format(), opts, (error, data, response) => {
      if (error) {
        if (typeof response === 'undefined') {
            document.getElementById("phasor_error").innerHTML = "<p class='alert alert-danger'><b>Connection error.</b> Try again later.</p>";
        }
        else if (response.statusCode == 403) {
          document.getElementById("limited").innerHTML = response.body;
        }
        else {
          console.error(error);
        }
      }
      else {
        this_object.setData(data);
      }
    });
  }


  // Set data and compute some statistics for the display
  this.setData = function(data) {
    this.data = [{}];
    this.data.x = data.index;
    // The data is returned as a matrix from the API
    var data_matrix = math.matrix(data.data);
    this.size = this.data.x.length;
    if (this.size == 0) {
      document.getElementById("phasor_date").innerHTML = "No data returned";
      return;
    }
    // Separate columns of the matrix
    this.data.v12 = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(V12)))._data);
    this.data.v23 = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(V23)))._data);
    this.data.v31 = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(V31)))._data);
    this.data.i1 = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(I1)))._data);
    this.data.i2 = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(I2)))._data);
    this.data.i3 = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(I3)))._data);
    this.data.cos = [].concat.apply([], data_matrix.subset(math.index(math.range(0, this.size), data.columns.indexOf(COS)))._data);
    
    // Fix the length of the slider bar
    document.getElementById("phasor_slider").max = this.size - 1;

    // Computing mean
    this.v_mean = MEAN_V;
    for(var i = 0; i < this.size; ++i) {
      // Ignoring aberrations 
      let v = phasor_solver(this.data.v12[i],
                            this.data.v23[i],
                            this.data.v31[i]);
      
      if(Math.abs(v[0]) < MAX_ADMISSIBLE_VOLTAGE)
        this.v_mean = v[0] / 2 + this.v_mean / 2;
      
      if(Math.abs(v[1]) < MAX_ADMISSIBLE_VOLTAGE)
        this.v_mean = v[1] / 2 + this.v_mean / 2;

      if(Math.abs(v[2]) < MAX_ADMISSIBLE_VOLTAGE)
        this.v_mean = v[2] / 2 + this.v_mean / 2;
    }
    this.setState(0);
  }


  // Set canvas on which the phasor will be displayed
  this.setCanvas = function (canvas) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext("2d");
    trackTransforms(this.ctx);

    /*
    * Adding event listeners
    */
    let ctx = this.ctx;
    let mouse_pos = {
      x: canvas.width * 0.42,
      y: lastY = canvas.height * 0.5
    }

    let drag_pos,
        dragging = false;

    // Get exact mouse position on the canvas
    function get_mouse_pos(evt) {
      let rect = this.canvas.getBoundingClientRect(),
        scaleX = this.canvas.width / rect.width,
        scaleY = this.canvas.height / rect.height;

      return {
        x: (evt.clientX - rect.left) * scaleX,
        y: (evt.clientY - rect.top) * scaleY
      }
    }

    this.canvas.addEventListener('mousedown', function(evt) {
      mouse_pos = get_mouse_pos(evt);

      drag_pos = ctx.transformPoint(mouse_pos);
      dragging = false;
    }, false);

    this.canvas.addEventListener('mousemove', function(evt) {
      mouse_pos = get_mouse_pos(evt);
      dragging = true;

      if(drag_pos) {
        let p = ctx.transformPoint(mouse_pos);
        ctx.translate(p.x - drag_pos.x, p.y - drag_pos.y);
        PHASOR_DIAGRAM.updatePhasor();
      }
    }, false);

    // Scales the canvas with a factor 1.1^ticks
    function scale_phasor(ticks) {
      let factor = Math.pow(1.1, ticks);
      let p = ctx.transformPoint(mouse_pos);

      ctx.translate(p.x, p.y);
      ctx.scale(factor, factor);
      ctx.translate(- p.x, - p.y);
      PHASOR_DIAGRAM.updatePhasor();
    }

    this.canvas.addEventListener('mouseup',function(evt){
      drag_pos = null;

      if(!dragging)
        scale_phasor(evt.shiftKey ? -1 : 1 );
    }, false);

    // Mouse scroll handler for firefox
    this.canvas.addEventListener('DOMMouseScroll', function(evt){
      evt.preventDefault();
      scale_phasor(- evt.detail);
    }, false);

    // Mouse scroll handler for other browsers
    this.canvas.addEventListener("mousewheel", function(evt) {
      evt.preventDefault();
      scale_phasor(evt.wheelDelta / 40);
    }, false);
  }

  // Update the phasor display and values
  this.updatePhasor = function() {
    if(this.data === null)
      return;

    // Clear entire canvas
    let p1 = this.ctx.transformPoint({x: 0, y: 0});
    let p2 = this.ctx.transformPoint({x: this.canvas.width, 
                                       y: this.canvas.height});
    this.ctx.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);

    // get canvas information
    let center = [this.canvas.width * 0.5, this.canvas.height * 0.5];

    // vector names
    let v_names = ['V₁', 'V₂', 'V₃'],
        I_names = ['I₁', 'I₂', 'I₃'],
        u_names = ['U₁₂', 'U₂₃', 'U₃₁'];

    // multiplier to exagerate fluctuations and normalizers for rendering
    let v_mult = 7,
        v_norm = 0.5 * this.canvas.width / 12500,
        I_norm = 0.5 * this.canvas.width / 35;

    // retrieve the information needed to draw the phasor
    let angle = Math.acos(this.data.cos[this.state]),
        I = [this.data.i1[this.state],
             this.data.i2[this.state],
             this.data.i3[this.state]],
        v = phasor_solver(this.data.v12[this.state],
                          this.data.v23[this.state],
                          this.data.v31[this.state]);
    
    // aberrations handling
    for(var i = 0; i < 3; ++i) {
      if(Math.abs(v[i]) > MAX_ADMISSIBLE_VOLTAGE)
        v[i] = this.v_mean;
    }

    // modifying data for the rendering
    let P = 0,
        Q = 0,
        I_len = [0, 0, 0],
        v_len = [0, 0, 0];
    for(var i = 0; i < 3; ++i) {
      // Amplifying the difference in length of displayed voltage vectors
      // around the mean
      v_len[i] = (this.v_mean + v_mult * (v[i] - this.v_mean)) * v_norm;
      I_len[i] = I[i] * I_norm;

      // Computing the unphased active powers
      P += I[i] * v[i];
    }
    // Computing the activa and reactive powers
    Q = P * Math.sin(angle) / Math.sqrt(3);
    P = P * Math.cos(angle) / Math.sqrt(3);

    /* Rendering new phasor */
    // draw vectors
    for(var i = 0; i < 3; ++i) {
      let rot = - 2 * i * Math.PI / 3;

      // draw inner tension vectors
      phasor_draw_arrow(canvas, center,
        [center[0] + v_len[i] * Math.cos(rot),
         center[1] - v_len[i] * Math.sin(rot)],
         'black',
         v_names[i],
         'left',
         false);

      // draw current vectors
      phasor_draw_arrow(canvas, center,
        [center[0] + I_len[i] * Math.cos(rot - angle),
         center[1] - I_len[i] * Math.sin(rot - angle)],
         'red',
         I_names[i],
         'right',
         false);

      // draw drashed tensions
      if(i == 2)
        phasor_draw_arrow(canvas,
          [center[0] + v_len[0] * Math.cos(rot - 2 * Math.PI / 3),
           center[1] - v_len[0] * Math.sin(rot - 2 * Math.PI / 3)],
          [center[0] + v_len[2] * Math.cos(rot),
           center[1] - v_len[2] * Math.sin(rot)],
          'blue',
          u_names[i],
          'right',
          true);
      else
        phasor_draw_arrow(canvas,
          [center[0] + v_len[i + 1] * Math.cos(rot - 2 * Math.PI / 3),
           center[1] - v_len[i + 1] * Math.sin(rot - 2 * Math.PI / 3)],
          [center[0] + v_len[i] * Math.cos(rot),
           center[1] - v_len[i] * Math.sin(rot)],
          'blue',
          u_names[i],
          'right',
          true);
    }

    // Updating values
    document.getElementById("phasor_u12").innerText = (this.data.v12[this.state] / 1000).toFixed(3);
    document.getElementById("phasor_u23").innerText = (this.data.v23[this.state] / 1000).toFixed(3);
    document.getElementById("phasor_u31").innerText = (this.data.v31[this.state] / 1000).toFixed(3);
    document.getElementById("phasor_I1").innerText = I[0].toFixed(3);
    document.getElementById("phasor_I2").innerText = I[1].toFixed(3);
    document.getElementById("phasor_I3").innerText = I[2].toFixed(3);
    document.getElementById("phasor_theta").innerText = angle.toFixed(3);
    document.getElementById("phasor_P").innerText = (P / 1000).toFixed(3);
    document.getElementById("phasor_Q").innerText = (Q / 1000).toFixed(3);
    document.getElementById("phasor_date").innerText = moment(
      this.data.x[this.state]).format('dddd Do MMMM YYYY, HH:mm');
  }
}


/*******************************************************************************
 PhasorPlayer class
*******************************************************************************/


/**
 * A class containing methods to control the phasor player
 *
 */
function PhasorPlayer() {
  this.time_step = 128;
  this.time_handler = null;

  this.toggle = function() {
    if(this.time_handler)
      this.stop();
    else
      this.run();
  }

  this.run = function() {
    let size = PHASOR_DIAGRAM.getSize();
    let new_state = PHASOR_DIAGRAM.getState() + 1;

    if(new_state >= size)
      new_state = 0;
    
    PHASOR_DIAGRAM.setState(new_state);
    this.time_handler = setTimeout(this.run.bind(this), this.time_step);
  }
  
  this.stop = function() {
    clearTimeout(this.time_handler);
    this.time_handler = null;
  }

  this.reset = function() {
    if(this.time_handler) {
      clearTimeout(this.time_handler);
      this.time_handler = null;
    }
    this.time_step = 128;
    PHASOR_DIAGRAM.setState(0);
  }

  this.accelerate = function() {
    this.time_step = Math.ceil(this.time_step / 2);

    if(this.time_handler) {
      clearTimeout(this.time_handler);
      this.run();
    }
  }

  this.decelerate = function() {
    this.time_step *= 2;
  }
}


/*******************************************************************************
 Helper functions
*******************************************************************************/


/**
 * Modifies and add methods so that a context's tranforms are tracked.
 *
 * @param {ctx} ctx A context object whose methods 'scale' and 'translate' are
 *                  modified so that the transforms are tracked through ctx's
 *                  new method 'transformPoint(x)' that takes as argument a
 *                  point x and returns a SVG corresponding to x's coordinates
 *                  in the tranformed context coordinate system.
 */
function trackTransforms(ctx) {
  // Loading Scalable Vector Graphics from the w3.org svg XML namespace
  let svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
  let xform = svg.createSVGMatrix();

  let scale = ctx.scale;
  ctx.scale = function(s, s) {
    xform = xform.scaleNonUniform(s, s);
    scale.call(ctx, s, s);
  };

  let translate = ctx.translate;
  ctx.translate = function(dx, dy) {
    xform = xform.translate(dx, dy);
    translate.call(ctx, dx, dy);
  };

  ctx.transformPoint = function(p) {
    let svg_p  = svg.createSVGPoint();
    svg_p.x = p.x;
    svg_p.y = p.y;
    return svg_p.matrixTransform(xform.inverse());
  }
}


/**
 * Solves the equations for the inner voltages.
 *
 * Solves the non linear system of equations
 *    v12**2 = v1**2 + v2**2 + v1 * v2,
 *    v23**2 = v2**2 + v3**2 + v2 * v3,
 *    v31**2 = v3**2 + v1**2 + v3 * v1,
 * for (v1, v2, v3) given a triplet (v12, v23, v31).
 *
 * @param {number} v12          v12 parameter (see descripion).
 * @param {number} v23          v23 parameter (see descripion).
 * @param {number} v31          v31 parameter (see descripion).
 * @param {array}  [v1, v2, v3] Solution (see descripion).
 */
var v1_guess = 8660;
function phasor_solver(v12, v23, v31) {

  // No solution, returns an approximation of the last one
  if(v12 > v23 + v31 || v23 > v12 + v31 || v31 > v12 + v23)
    return [v1_guess, v1_guess, v1_guess];

  // Scaling back
  if(v12**2 < 0.75 * v1_guess**2)
    v1_guess = v12 / Math.sqrt(3);

  // Actually solving
  [v1, v2, v3] = [0, 0, 0];
  for(var i = 0; i < 1000; ++i) {
    v2 = - 0.5 * v1_guess + Math.sqrt(v12**2 - 0.75 * v1_guess**2);
    v3 = - 0.5 * v2 + Math.sqrt(v23**2 - 0.75 * v2**2);
    v1 = - 0.5 * v3 + Math.sqrt(v31**2 - 0.75 * v3**2);
    if (Math.abs(v1 - v1_guess) < 1)
      break;
    else
      v1_guess = (v1 + v1_guess) / 2;
  }

  return [v1, v2, v3];
}


/**
 * Draw an arrow on the given canvas, starting from the center of the canvas.
 *
 * @param {canvas}  canvas    The HTML5 canvas to draw on.
 * @param {array}   pos1      Start position of the arrow.
 * @param {array}   pos2      End position of the arrow.
 * @param {str}     style     The color to draw the arrow in.
 * @param {str}     name      Name of the arrow.
 * @param {str}     name_side "left" or "right", side on which the name is
 *                            displayed.
 * @param {bool}    dashed Is the arrow dashed or not.
 */
function phasor_draw_arrow(canvas, pos1, pos2, style, name, name_side, dashed) {
  let ctx = canvas.getContext("2d");
  ctx.fillStyle = style;
  ctx.strokeStyle = style;
  ctx.lineWidth = 2.75;

  // Computing unit vect
  let u_vect = [pos2[0] - pos1[0], pos2[1] - pos1[1]],
      u_norm = Math.sqrt(u_vect[0] ** 2 + u_vect[1] ** 2);
  u_vect = [u_vect[0] / u_norm, u_vect[1] / u_norm];

  /* draw the tip of the arrow */
  // Some geometry
  let tip_size = 30,
      tip_angle = 0.2,
      tip_sin = Math.sin(tip_angle),
      tip_cos = Math.cos(tip_angle),
      tip1_vect = [tip_size * (u_vect[0] * tip_cos - u_vect[1] * tip_sin),
                  tip_size * (u_vect[0] * tip_sin + u_vect[1] * tip_cos)],
      tip2_vect = [tip_size * (u_vect[0] * tip_cos + u_vect[1] * tip_sin),
                  tip_size * (- u_vect[0] * tip_sin + u_vect[1] * tip_cos)];

  ctx.beginPath();
  ctx.moveTo(pos2[0], pos2[1]);
  ctx.lineTo(pos2[0] - tip1_vect[0], pos2[1] - tip1_vect[1]);
  ctx.lineTo(pos2[0] - tip2_vect[0], pos2[1] - tip2_vect[1]);
  ctx.closePath();
  ctx.fill();


  /* draw the vector itself */
  ctx.beginPath();
  if(dashed)
    ctx.setLineDash([5, 15]);
  else
    ctx.setLineDash([]);

  ctx.moveTo(pos1[0], pos1[1]);
  ctx.lineTo(pos2[0] - tip_size * tip_cos * u_vect[0],
             pos2[1] - tip_size * tip_cos * u_vect[1]);
  ctx.closePath();
  ctx.stroke();

  /* Name placement */
  let name_dist = 0,
      name_pos,
      name_scal_v,
      name_scal_h,
      u_rot;

  if(name_side == 'right') {
    u_rot = [- u_vect[1], u_vect[0]];
    name_scal_v = u_vect[1];
    name_scal_h = u_vect[0];
  }
  else { 
    u_rot = [u_vect[1], - u_vect[0]];
    name_scal_v = - u_vect[1];
    name_scal_h = - u_vect[0];
  }

  name_dist += 20 + name_scal_h * 15;
  name_dist += name_scal_v > 0 ? name_scal_v * name.length * 20 : 0;

  name_pos = [pos1[0] + u_vect[0] * u_norm / 2 + u_rot[0] * name_dist,
              pos1[1] + u_vect[1] * u_norm / 2 + u_rot[1] * name_dist];

  ctx.font = '50px sans-serif';
  ctx.fillStyle = style;
  ctx.fillText(name, name_pos[0], name_pos[1]);
}


/*******************************************************************************
 Functions of the Sidebar
*******************************************************************************/


// Handles the phasor side bar (i.e. updates the PHASOR_DIAGRAM's data)
function phasor_sidebar_handler() {
  // Retrieve the building name from the building-dropdown-list
  let building = document.getElementById("phasor_building").value;

  // if in manual mode, then also read from- and to-date
  let from = null, to = null;

  // Retrieve the from date and time
  let fromDate = document.getElementById("phasor_from_date").value;
  let fromTime = document.getElementById("phasor_from_time").value;
  from = moment(fromDate + ' ' + fromTime, "DD/MM/YYYY HH:mm");

  // Retrieve the to date and time
  let toDate = document.getElementById("phasor_to_date").value;
  let toTime = document.getElementById("phasor_to_time").value;
  to = moment(toDate + ' ' + toTime, "DD/MM/YYYY HH:mm");

  // Alert the user if the dates are not correct
  if (from.isAfter(to)) {
    alert('Error: The "from" date should be a date that comes before the "to" date.');
    return
  }

  PHASOR_DIAGRAM.loadData(new DataInfo(building, "Phasor", from, to));
}