/**
 * ELECTRICAL ENERGY AT A GLANCE
 *
 * Authors: Team 2:
 * Maxence CALIXTE, Saul ESCALONA, Adrien SCHOFFENIELS
 * Tanguy SPITS, Arian TAHIRAJ, Dominik ZIANS
 * 
 * This file contains functions used by addBuilding.html
 */

/*****************************************************************************
 Form handlers
*****************************************************************************/

// Generates the 'type' selector on basis of the existing types
// Also creates a dictionnary that associates each type with
// its datatypes
var types = {};
BUILDINGS.forEach( function (building) {
	// Should only take each type once
	if (building.type in types) {
		return
	}
	// Add the option to the selector
	document.getElementById("type").innerHTML += "<option value='" + building.type + "'>" + building.type + "</option>";
	// Add the datatypes to the dictionary
	types[building.type] = building.datatypes;
});

// On load, click on the type selector to laod the infos
clickType();

// Show the datatypes in the select according to the type selected
// and show the datatypes for the min and max values
function clickType()  {
	document.getElementById("mapDatatype").innerHTML = '';
	types[document.getElementById("type").value].forEach( function (datatype) {
		document.getElementById("mapDatatype").innerHTML += "<option value='" + datatype + "'>" + datatype + "</option>";
	});
	document.getElementById("datatypes").innerHTML = types[document.getElementById("type").value].join(", ");
}

// Change buttons dynamically depending on the type of action
// And set the current values if the action is modify/delete
function toggleButtons() {
  if (document.getElementById("building").value == "addBuilding") {
    // Set buttons
    document.getElementById("addButton").style.display = "block";
    document.getElementById("modifyButtons").style.display = "none";

    // Set content of forms to empty
	document.getElementById("buildingName").value = ''
	document.getElementById("buildingDisplayName").value = ''
	document.getElementById("location").value = '';
	document.getElementById("lastAdded").value = '';
  } 
  else {
  	// Set buttons
    document.getElementById("addButton").style.display = "none";
    document.getElementById("modifyButtons").style.display = "block";

    // Set content of forms to current value
    BUILDINGS.forEach( function (building) {
    	if (building.name == document.getElementById("building").value) {
    		document.getElementById("buildingName").value = building.name;
    		document.getElementById("buildingDisplayName").value = building.display_name;
    		document.getElementById("location").value = building.location.join(", ");
    		document.getElementById("mapDatatype").value = building.map_datatype;
    		document.getElementById("datatypes").value = building.datatypes;
    		document.getElementById("maximas").value = building.maxima.join(", ");
    		document.getElementById("minimas").value = building.minima.join(", ");
    		document.getElementById("lastAdded").value = moment(building.last_added).format("DD/MM/YYYY HH:mm");

    	}
    })
    document.getElementById("buildingName").value = document.getElementById("building").value;
  }
}


/*****************************************************************************
 Requests handelrs
*****************************************************************************/

// Prepare the BuildingInfo object to send it to the API
function setBuildingInfo() {
  building_info.name = document.getElementById("buildingName").value;
  var location = document.getElementById("location").value;
  var loc = location.split(",");
  building_info.location = [parseFloat(loc[0]), parseFloat(loc[1])];
  building_info.displayName = document.getElementById("buildingDisplayName").value;
  building_info.mapDatatype = document.getElementById("mapDatatype").value;
  building_info.type = document.getElementById("type").value;
  building_info.datatypes = types[building_info.type];
  building_info.maxima = document.getElementById("maximas").value.split(", ").map(x=>+x);
  building_info.minima = document.getElementById("minimas").value.split(", ").map(x=>+x);
  building_info.lastAdded = moment(document.getElementById("lastAdded"), "DD/MM/YYYY HH:mm");
}

// Send request to add building to the API
function addBuilding() {
  setBuildingInfo();
  data_api.addBuilding(building_info, (error, data, response) => {
    if (error) {
      console.error(error);
      alert("An error occured.");
    }
    else {
      alert("Building successfully added to the API.");
      document.getElementById("refresh").click();
    }
  });
}

// Send request to modify a building to the API
function modifyBuilding() {
  setBuildingInfo();
  console.log(building_info);
  data_api.updateBuildingInfo(building_info.name, building_info, (error, data, response) => {
    if (error) {
      console.error(error);
      alert("An error occured.");
    }
    else {
      alert("Building successfully modified.");
      document.getElementById("refresh").click();
    }
  });
}

// Send request to delete a building from the API
function deleteBuilding() {
  data_api.deleteBuilding(document.getElementById("buildingName").value, (error, data, response) => {
    if (error) {
      console.error(error);
      alert("An error occured.");
    }
    else {
      alert("Building successfully deleted");
      document.getElementById("refresh").click();
    }
  });
}
