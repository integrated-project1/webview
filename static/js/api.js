// Assigns the API functions to the window so that they can be used everywhere
var ElectricalEnergyAtAGlanceApi = require('electrical_energy_at_a_glance_api');

window.data_api = new ElectricalEnergyAtAGlanceApi.DefaultApi();
window.building_info = new ElectricalEnergyAtAGlanceApi.BuildingInfo();