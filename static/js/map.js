/**
 * ELECTRICAL ENERGY AT A GLANCE
 *
 * Authors: Team 2:
 * Maxence CALIXTE, Saul ESCALONA, Adrien SCHOFFENIELS
 * Tanguy SPITS, Arian TAHIRAJ, Dominik ZIANS
 * 
 * This file contains classes and functions for drawing the map inluding
 * cables and cabins
 */


/*****************************************************************************
 Constants
*****************************************************************************/

// Leaflet map options

var MAP_ID = "map";
var MAP_INITIAL_VIEW = [50.586482, 5.564652];
var MAP_INITIAL_ZOOM = 17;
var MAP_URL = 'https://api.mapbox.com/styles/v1/mapbox/light-v8/tiles/{z}/{x}/{y}';
var MAP_ACCESS_TOKEN = 'pk.eyJ1IjoiYXNjaG9mZmVuaWVscyIsImEiOiJjazMzZ2hmb2YwcTc2M25wYjVjZDNqdGxsIn0.75PscnmXmQQg6cmibknawQ';
var MAP_ATTRIBUTION = '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> & <a href="https://www.mapbox.com/">Mapbox</a>'
var MAP_MAX_ZOOM = 18;
var MAP_MIN_ZOOM = 15;
var MAP_TILE_SIZE = 512;
var MAP_ZOOM_OFFSET = -1;
var MAP_ZOOM_DELTA = 0.5;
var MAP_ZOOM_SNAP = 0.25;


// Markers on maps options

var CABIN_ICON = 'glyphicon glyphicon-flash';
var CABLE_COLOR = 'red';
var CABLE_WEIGHT = 3;
var CABLE_JSON = "/static/js/cables.geojson";
var LEGEND_POSITION = "bottomright";
var BUILDING_ICON = "glyphicon glyphicon-home";


// Layout of the diagrams

const MAP_DIAGRAM_LAYOUT = {
  autosize: true,
  height: 175,
  width: 340,
  showlegend: false,
  margin: {
    l: 28,
    r: 10,
    b: 40,
    t: 5,
    pad: 0
  },
  xaxis: {
    showline: true,
    linewidth: 1,
    showgrid: false,
    linecolor: 'rgb(204,204,204)',
    autotick: false,
    ticks: 'inside',
    tickcolor: 'rgb(204,204,204)',
    tickwidth: 1,
    nticks:3,
    tickmode:'auto',
    ticklen: 5,
    tickfont: {
      size: 12,
      color: 'rgb(82, 82, 82)'
    }
  },
  yaxis: {
    showline: true,
    linewidth: 1,
    showgrid: false,
    linecolor: 'rgb(204,204,204)',
    autotick: false,
    ticks: 'inside',
    tickmode: 'auto',
    nticks: 3,
    tickcolor: 'rgb(204,204,204)',
    tickwidth: 1,
    ticklen: 5,
    tickfont: {
      size: 12,
      color: 'rgb(82, 82, 82)'
    }
  }
};


// Options of the diagram in the data-tab

const MAP_DIAGRAM_OPTIONS = {
  displaylogo : false,
  displayModeBar : false
};

/*****************************************************************************
 Content
*****************************************************************************/

// Creation of the map

var map = L.map(MAP_ID, {
  fullscreenControl: {pseudoFullscreen: true},
  scrollWheelZoom: false,
  zoomDelta: MAP_ZOOM_DELTA,
  zoomSnap: MAP_ZOOM_SNAP,
  gestureHandling: true,
  gestureHandlingOptions: {duration: 750}
}).setView(MAP_INITIAL_VIEW, MAP_INITIAL_ZOOM);

L.tileLayer(MAP_URL + '@2x?access_token=' + MAP_ACCESS_TOKEN, {
  tileSize: MAP_TILE_SIZE,
  minZoom: MAP_MIN_ZOOM,
  maxZoom: MAP_MAX_ZOOM,
  zoomOffset: MAP_ZOOM_OFFSET,
  attribution: MAP_ATTRIBUTION
}).addTo(map);


// Addition of 15 kV cables and HV-cabins on the map

$.getJSON(CABLE_JSON, function(data) {
  L.geoJson(data, {
    filter: function(feature) {
      //Displays HV-cabins only if user connected
      if (feature.geometry.type === "Point") {
        return connected
      } else {
        return true;
      }
    },
    pointToLayer: function(feature, latlng) {
      return L.marker(latlng, {
        icon: L.divIcon( {
          className: CABIN_ICON,
          iconAnchor: [10,10]
        })
      });
    },
    style: function(feature) {
      return {
        color: CABLE_COLOR,
        weight: CABLE_WEIGHT
      };
    }
  }).addTo(map);
});


// Addition of the legend

var legend = L.control({position: LEGEND_POSITION});
legend.onAdd = function(map) {
  var div = L.DomUtil.create("div", "legend");
  div.innerHTML += '<p><i style="background:'+CABLE_COLOR+'"></i>15-kV cable</p>';
  div.innerHTML += '<p><span class="'+BUILDING_ICON+' legend-icon"></span>Building</p>'
  if (connected) {
    div.innerHTML += '<p><span class="'+CABIN_ICON+' legend-icon"></span>HV-cabin</p>';
  }
  return div;
};
legend.addTo(map);


// Addition of the options on the map

var timeOption = L.control({position: "topleft"});
timeOption.onAdd = function(map) {
  var div = L.DomUtil.create("div", "leaflet-bar leaflet-options dropdown");
  div.innerHTML += `<a href="#" data-toggle="dropdown" title="Select time interval of the data">
    <span class="glyphicon glyphicon-time"></span>
    </a>
    <ul class="dropdown-menu">
    <li onclick="changeInterval(15, 'minute')">15m</li>
    <li onclick="changeInterval(1, 'hour')">1h</li>
    <li onclick="changeInterval(4, 'hour')">4h</li>
    <li onclick="changeInterval(8, 'hour')">8h</li>
    <li onclick="changeInterval(12, 'hour')">12h</li>
    <li onclick="changeInterval(1, 'day')">1day</li>
    <li onclick="changeInterval(1, 'week')">1week</li>
    </ul>
     `;
  return div;
};
timeOption.addTo(map);


var buildingOption = L.control({position: "topleft"});
buildingOption.onAdd = function(map) {
  var div = L.DomUtil.create("div", "leaflet-bar leaflet-options dropdown");
  div.innerHTML += document.getElementById("buildingDrop").innerHTML;
  return div;
};
buildingOption.addTo(map);


// Addition of the buildings on the map

BUILDINGS.forEach(function (building) {
    MAP_DIAGRAMS[building.name] = new Diagram(
      building.name, MAP_DIAGRAM_LAYOUT, MAP_DIAGRAM_OPTIONS, false);


    // Contains what the popup will display (the plot of the power consumption).
    var popup = `<div class='panel panel-default' style = 'width:350px'>`
      + `<div class='panel-heading panel-heading-custom'><h5>${building.display_name} power ${building.type} (kW)</h5></div>`
      + `<div class='panel-body' id=${building.name} style = 'padding:3px;'>`
      + `</div></div>`;

    var marker = new L.Marker(building.location, {
      title: building.displayName,
      icon: L.divIcon({className: BUILDING_ICON, iconAnchor: [13,-3]})
    })
    .bindPopup(popup, {autoClose : false, closeOnClick: false, maxWidth:500})
    .on('popupopen', function () {
      MAP_DIAGRAMS[building.name].startLiveDisplay();
    })
    .on('popupclose', function () {
      MAP_DIAGRAMS[building.name].pauseLiveDisplay();
    });

    MAP_MARKERS[building.name] = marker.addTo(map);

    var diagram = MAP_DIAGRAMS[building.name];
    diagram.setLiveDisplay(new DataInfo(building.name, building.map_datatype, null, null), 1, 'day');
  
    diagram.pauseLiveDisplay();
  });


// Open a random popup

MAP_MARKERS[BUILDINGS[Math.floor(Math.random() * Object.keys(BUILDINGS).length)].name].openPopup();


/**
 * Changes the diagrams' intervals.
 * 
 * @param {number} shift - The number of units to shift the date
 * @param {string} unit - The unit to shift the Date by ('minute', 'hour', 'day', 'week', 'month', 'year')  
 */
function changeInterval(shift, unit) {
  for(var key in MAP_DIAGRAMS) {
    MAP_DIAGRAMS[key].setLiveDisplay(null, shift, unit);
    if(MAP_MARKERS[key].getPopup().isOpen()) {
      MAP_DIAGRAMS[key].startLiveDisplay();
    }
  };
}