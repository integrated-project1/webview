/**
 * ELECTRICAL ENERGY AT A GLANCE
 *
 * Authors: Team 2:
 * Maxence CALIXTE, Saul ESCALONA, Adrien SCHOFFENIELS
 * Tanguy SPITS, Arian TAHIRAJ, Dominik ZIANS
 * 
 * This file contains functions used by plots.html, and creates
 * the diagram shown in the page
 */


/*****************************************************************************
 Constants
*****************************************************************************/
// Layout of the diagram in the data-tab
const DATA_DIAGRAM_LAYOUT = {
  xaxis : {
    type : "date"
  }
};


// Options of the diagram in the data-tab
const DATA_DIAGRAM_OPTIONS = {
  responsive : true,
  displaylogo : false,
  displayModeBar : true
};


/*****************************************************************************
 Creation of the diagram
*****************************************************************************/

const DATA_DIAGRAM = new Diagram("plot", DATA_DIAGRAM_LAYOUT, DATA_DIAGRAM_OPTIONS, true);


/*****************************************************************************
 Functions of the Sidebar
*****************************************************************************/


/**
 * Ensure that all the elements of the sidebar show the correct content
 */
function updateSidebar() {
  clickBuilding();
  clickCheckboxLive();
  clickCheckboxCompare();
}


/**
 * Handler fo when the user selected a building from the dropdown menu
 * This is needed because buildings have other data types than the photovoltaic unit
 */
function clickBuilding() {

  // first check which building was selected
  var data_building = document.getElementById("data_building").value;

  // then change the content of the dropdown menu and select the default
  var dt = document.getElementById("data_datatype");
  dt.innerHTML = "";
  DATATYPES[data_building].forEach( function (dataType) {
    dt.innerHTML += '<option value="' + dataType +'">' + dataType + '</option>';
  });
}


/**
 * Handler fo when the user switched between live and manual mode
 */
function clickCheckboxLive() {
  if(document.getElementById("checkbox_live").checked) { // switched to live-mode
    // hide menu for selecting date & time
    document.getElementById("manual_mode").style.display = "none";
    // show menu for selecting the interval
    document.getElementById("live_mode").style.display = "block";
    // hide date button if no more date selector in options
    if(!document.getElementById("checkbox_compare").checked) {
      document.getElementById("dateButton").style.display = "none";
    }
  }
  else { // switched to manual-mode
    // show menu for selecting date & time
    document.getElementById("manual_mode").style.display = "block";
    // hide menu for selecting the interval
    document.getElementById("live_mode").style.display = "none";
    // show date button
    document.getElementById("dateButton").style.display = "block";
  }
}


/**
 * Handler for when the user clicked on the compare checkbox
 */
function clickCheckboxCompare() {
  if(document.getElementById("checkbox_compare").checked) {
    // show the menu to select the data to compare with
    document.getElementById('compare_menu').style.display = "block";
    // show date button
    document.getElementById("dateButton").style.display = "block";
  }
  else {
    // hide the menu to select the data to compare with
    document.getElementById('compare_menu').style.display = "none";
    // hide date button if no more date selector in options
    if(document.getElementById("checkbox_live").checked) {
      document.getElementById("dateButton").style.display = "none";
    }
  }
}


/**
 * Handler for the 'Show data' button
 */
function data_sidebar_handler() {
  // Retrieve the building name from the building-dropdown-list
  var building = document.getElementById("data_building").value;

  // Retrieve the datatype name from the datatype-dropdown-list
  var datatype = document.getElementById("data_datatype").value;

  // if in manual mode, then also read from- and to-date
  var from = null, to = null, live_shift = null, live_unit = null;
  if(!document.getElementById("checkbox_live").checked) {
    // Retrieve the from date and time
    var fromDate = document.getElementById("data_from_date").value;
    var fromTime = document.getElementById("data_from_time").value;
    from = moment(fromDate + ' ' + fromTime, "DD/MM/YYYY HH:mm");

    // Retrieve the to date and time
    var toDate = document.getElementById("data_to_date").value;
    var toTime = document.getElementById("data_to_time").value;
    to = moment(toDate + ' ' + toTime, "DD/MM/YYYY HH:mm");

    // Alert the user if the dates are not correct
    if (from.isAfter(to)) {
      alert('Error: The "from" date should be a date that comes before the "to" date.');
      return
    }
  }
  else {
    live_shift = document.getElementById("live_shift_number").value;
    live_unit = document.getElementById("live_unit_list").value;
    live_shift = parseInt(live_shift, 10);
    if(isNaN(live_shift)) {
      live_shift = 1;
    }
  }

  // Retrieve from-date of comparison data
  var fromCompare = null;
  if(document.getElementById("checkbox_compare").checked) {

    // Get the date of the first compare data
    var fromCompareDate = document.getElementById("compare_from_date").value;
    var fromCompareTime = document.getElementById("compare_from_time").value;
    fromCompare = moment(fromCompareDate + ' ' + fromCompareTime, 'DD/MM/YYYY HH:mm');
  }

  if(fromCompare) {
    DATA_DIAGRAM.setCompareInfo(fromCompare);
  }
  else {
    DATA_DIAGRAM.clearCompareInfo();
  }

  if(live_shift) { // live mode
    DATA_DIAGRAM.setLiveDisplay(
      new DataInfo(building, datatype),
      live_shift,
      live_unit
    );
    // Set the title of the diagram
    DATA_DIAGRAM.layout.title = {
      text: 'Live ' + document.getElementById("data_building").options[document.getElementById("data_building").selectedIndex].text + ' ' + datatype + ' (' + DATA_DIAGRAM.info.getUnit(datatype) + ')'
    };
    DATA_DIAGRAM.startLiveDisplay();
  }
  else { // manual mode
    DATA_DIAGRAM.pauseLiveDisplay();
    DATA_DIAGRAM.setInfo(new DataInfo(building, datatype, from, to));
    // Set the title of the diagram
    DATA_DIAGRAM.layout.title = {
      text: document.getElementById("data_building").options[document.getElementById("data_building").selectedIndex].text + ' ' + datatype + ' (' + DATA_DIAGRAM.info.getUnit(datatype) + ')'
    };
    DATA_DIAGRAM.loadData();
  }
}
