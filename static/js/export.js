/**
 * ELECTRICAL ENERGY AT A GLANCE
 *
 * Authors: Team 2:
 * Maxence CALIXTE, Saul ESCALONA, Adrien SCHOFFENIELS
 * Tanguy SPITS, Arian TAHIRAJ, Dominik ZIANS
 * 
 * This file contains functions to export data and to handle
 * the export.html template
 */

/*******************************************************************************
 Constants
*******************************************************************************/

// Name of the file to download
const FILE_NAME = 'nyk_export.csv';


/*******************************************************************************
 Functions
*******************************************************************************/

/**
 * Return an array containing all the selected data types to export
 * @param {string} - The name of the div containing the checkboxes.
 * @return {string} - The datatypes selected separated with a space.
 */
function getSelectedChbox(div) {
  var selchbox = [];  // array that will store the value of selected checkboxes
  
  // Gets all the input tags in div, and their number
  var inpfields = div.getElementsByTagName('input');

  // Traverse the inpfields elements, and adds the value of selected (checked) checkbox in selchbox
  for(var i=0; i<inpfields.length; i++) {
    if(inpfields[i].type == 'checkbox' && inpfields[i].checked == true)
      selchbox.push(inpfields[i].value);
  }

  // Create the string separated with a whitespace
  return selchbox.join(' ');
}


/**
 * Handler fo when the user selected a building from the dropdown menu
 */
function clickBuildingExport() {

  // First check which building was selected
  var export_building = document.getElementById("export_building").value;


  // Then change the content of the check boxes
  var dt = document.getElementById("datatypecheck");
  dt.innerHTML = "";
  DATATYPES[export_building].forEach( function(dataType) {
    dt.innerHTML += '<div class="checkbox"><label><input type="checkbox" checked value="' + dataType + '">' + dataType + '</label></div>'
  });
}


/**
 * Handler for the 'Export data' button
 */
function data_export_handler() {
  // Retrieve the building name from the building-dropdown-list
  var building = document.getElementById("export_building").value;

  // Retrieve the datatypes name from the datatypes check boxes
  var checksDiv = document.getElementById("datatypecheck");
  var datatypes = getSelectedChbox(checksDiv);

  var from = null, to = null;
  // Retrieve the from date and time
  var fromDate = document.getElementById("data_from_date_export");
  var fromTime = document.getElementById("data_from_time_export");
  from = moment(fromDate.value + ' ' + fromTime.value, 'DD/MM/YYYY HH:mm');

  // Retrieve the to date and time
  var toDate = document.getElementById("data_to_date_export");
  var toTime = document.getElementById("data_to_time_export");
  to = moment(toDate.value + ' ' + toTime.value, 'DD/MM/YYYY HH:mm');


  // Alert the user if the dates are not correct
  if (from.isAfter(to)) {
    alert('Error: The "from" date should be a date that comes before the "to" date.');
    return
  }
  // Retrieve the time step
  var timestep = document.querySelector('input[name="timestep"]:checked').value;

  // Prepare the options of the request
  var opts = {
    "datatypes": datatypes,
    "timestep": timestep,
    "responseFormat": "csv"
  };

  // Request the csv to the API
  data_api.getData(building, from.format(), to.format(), opts, (error, data, response) => {
    if (error) {
      // If response is undefined, the connection to the API failed
      if (typeof response === 'undefined') {
        alert('Unable to connect to the API. Please try again later.');
      }
      // Not-authorized response
      else if (response.statusCode == 403) {
        alert(response.body);
      }
      // Unknown error
      else {
        console.error(error);
      }
    }
    else {
      // Download the csv
      var link = document.createElement('a');
      link.setAttribute('href', 'data:attachement/csv;charset=utf-8,' + encodeURI(response.body));
      link.setAttribute('download', FILE_NAME);
      link.click();
    }
  })
}
