import pytest
from auth.auth_utils import fetch_public_key, register_user, modify_password, update_user, delete_user, get_users, get_token, decode_token
import auth_client.openapi_client as auth_api
import os
import six
from webserver import app

@pytest.fixture
def client():
    app.config['SECRET_KEY'] = os.environ['CSRF_SECRET_KEY']
    with app.test_client() as client:
        token = get_token("admin", "lolilol")
        client.set_cookie('localhost', "access_token_cookie",token)
        yield client

def test_fetch_public_key(client):
    key = fetch_public_key()
    assert isinstance(key, six.string_types)

def test_register_user(client):
    result = register_user("test_client", "test_password", "read:data", "public")
    assert result == True

def test_modify_password(client):
    result = modify_password("test_password2")
    assert result == True

def test_update_user(client):
    result = update_user("test_client", "read:data")
    assert result == True

def test_delete_user(client):
    result = delete_user("test_client")
    assert result == True

def test_get_users(client):
    users = get_users()
    assert 'admin' in users

def test_get_token(client):
    token = get_token("admin", "lolilol")
    assert isinstance(token, six.string_types)
