import unittest
import pytest
from webserver import app
import os

@pytest.fixture
def client():
    app.config['SECRET_KEY'] = os.environ['CSRF_SECRET_KEY']
    with app.test_client() as client:
        yield client

def test_home(client):
    # Test if the homepage is returned
    r = client.get('/home')
    assert r.status_code == 200

def test_map(client):
    # Test if the homepage is returned
    r = client.get('/home')
    assert r.status_code == 200