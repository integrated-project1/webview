

FROM node:alpine

COPY --from=registry.gitlab.com/integrated-project1/the-main-api/javascript-client:master /usr/src/app /javascript-client
WORKDIR /javascript-client 
COPY static/js/api.js .

RUN cd /javascript-client && sed -i 's~http://localhost/1.0.0~/data~g' ./src/ApiClient.js && npm install && npm link && npm link /javascript-client  && npm run build
RUN npm install -g browserify 
RUN browserify api.js -o bundle.js



FROM python:3-alpine

RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

RUN mkdir -p /usr/src/app /usr/scr/app/auth_client /usr/scr/app/data_client
WORKDIR /usr/src/app

# Getting the auth client & installing requirements
COPY --from=registry.gitlab.com/integrated-project1/authentication-microservice/client:master /usr/src/app/openapi_client ./auth_client/openapi_client
COPY --from=registry.gitlab.com/integrated-project1/authentication-microservice/client:master /usr/src/app/requirements.txt ./auth_client/openapi_client/requirements.txt
RUN pip3 install --no-cache-dir -r ./auth_client/openapi_client/requirements.txt

# Getting the data client & installing requirements
COPY --from=registry.gitlab.com/integrated-project1/the-main-api/python-client:master /usr/src/app/openapi_client ./data_client/openapi_client
COPY --from=registry.gitlab.com/integrated-project1/the-main-api/python-client:master /usr/src/app/requirements.txt ./data_client/openapi_client/requirements.txt
RUN pip3 install --no-cache-dir -r ./data_client/openapi_client/requirements.txt

# Why can't we change a client's name at codegen...
RUN touch ./auth_client/__init__.py ./data_client/__init__.py \
	&& find ./auth_client -type f -name '*.py' | xargs sed -i 's~openapi_client~auth_client.openapi_client~g' \
	&& find ./data_client -type f -name '*.py' | xargs sed -i 's~openapi_client~data_client.openapi_client~g'

# Getting the server & installing requirements
COPY requirements.txt requirements.txt

RUN apk update \
    && apk add --no-cache --virtual mysqlclient-deps gcc python3-dev musl-dev \
    && apk add mariadb-dev \
    && apk add --no-cache --virtual cryptography-deps libressl-dev libffi-dev \
    && pip3 install --no-cache-dir -r requirements.txt \
    && apk del mysqlclient-deps cryptography-deps


COPY . /usr/src/app/

COPY --from=0 /javascript-client/bundle.js /usr/src/app/static/js/bundle.js

ENV JWT_ALGORITHM=RS256

STOPSIGNAL SIGINT

CMD ["python3", "webserver.py"]

# NB this is not optima at all
