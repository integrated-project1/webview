from typing import List

import jwt
import time
import os
from flask import request, jsonify

import auth_client.openapi_client as auth_api

# Configuring the authentication api client
configuration = auth_api.Configuration(host = os.environ['AUTH_SERV_URL_PORT'])

def fetch_public_key():
    """ 
    Requests the authentication server's public key and store it as 
    environment variable. 
    
    """
    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_api.DefaultApi(api_client)

        try:
            os.environ['PUBLIC_KEY'] = api_instance.get_key()[0]['key']
        except (auth_api.rest.ApiException) as e:
            print("Exception when calling DefaulApi->get_key: %s \n" % e)

def decode_token(token):
    """
    Validate and decode token.
    Returned value will be passed in 'token_info' parameter of your operation function, if there is one.
    'sub' or 'uid' will be set in 'user' parameter of your operation function, if there is one.
    'scope' or 'scopes' will be passed to scope validation function.

    :param token: Token provided by Authorization header
    :type token: str

    :return: Decoded token information or None if token is invalid
    :rtype: dict | None
    """
    try:
        payload = jwt.decode(token, os.environ['PUBLIC_KEY'], algorithms=[os.environ["JWT_ALGORITHM"]])

        # In order to be compliant with Connexion and RFC 7662
        payload['active'] = True if payload['exp'] < int(time.time()) else False
        return payload
    except Exception as e:
        print("Exception when decoding: %s \n" % e, flush=True)
        return None

def get_token(username, password):
    """
    Log a user in, and fetch the access token

    :param username: Name of the account of the user
    :type username: str
    :param password: Password of the user
    :type password: str

    :return: Decoded token information or None if token is invalid
    :rtype: dict | None
    """
    authentication_info = auth_api.AuthenticationInfo(
                client_id=username,
                client_secret=password,
                response_type='token')
    # Get the access token
    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        auth_api_instance = auth_api.DefaultApi(api_client)
        try:
            login = auth_api_instance.login(authentication_info)

            # Retrieving access token
            token = login[2]["Location"].split('#')[1].split('&')[0].split('=')[1]
            return token
        except (auth_api.rest.ApiException) as e:
            print("Exception when calling DefaulApi->login: %s \n" % e)
            return None

def register_user(username, password, scope, client_type):
    """ 
    Requests the authentication server to register a new user.

    :param username: Name of the account of the user to register
    :type username: str
    :param password: Password of the user to register
    :type password: str
    :param scope: List of scopes that the user will access
    :type scope: str
    :param client_type: 
    :type client_type: str

    :return: True if success, False if failed
    :rtype: Bool
    """
    token = request.cookies.get('access_token_cookie')

    # Passing token for authenticating user at auth server
    configuration.access_token = token
    
    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_api.DefaultApi(api_client)
        client = auth_api.Client(username, password, scope, client_type, redirect_uris="http://localhost/home")

        try:
            api_instance.register(client)
            return True
        except auth_api.rest.ApiException as e:
            print("Exception when calling DefaulApi->register: %s \n" % e, flush=True)
            return False

def modify_password(new_password):
    """ 
    Requests the authentication server to modify the current user password.

    :param new_password: Password of the user to register
    :type new_password: str


    :return: True if success, False if failed
    :rtype: Bool
    """
    token = request.cookies.get('access_token_cookie')

    # Passing token for authenticating user at auth server
    configuration.access_token = token

    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_api.DefaultApi(api_client)
        updated_client = auth_api.Client(client_secret=new_password)
        try:
            api_instance.update_my_info(updated_client)
            return True
        except (auth_api.rest.ApiException) as e:
            print("Exception when calling DefaulApi->update_my_info: %s \n" % e, flush=True)
            return False

def update_user(username, new_scope):
    """ 
    Requests the authentication server to update an user accounts scope.

    :param username: User to update
    :type username: str
    :param new_scope: new scope to attribute to the user
    :type username: str

    :return: True if success, False if failed
    :rtype: Bool
    """
    token = request.cookies.get('access_token_cookie')

    # Passing token for authenticating user at auth server
    configuration.access_token = token

    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_api.DefaultApi(api_client)
        updated_client = auth_api.Client(client_id=username, scope=new_scope)
        try:
            api_instance.update_client_info(username, updated_client)
            return True
        except (auth_api.rest.ApiException) as e:
            print("Exception when calling DefaulApi->update_client_info: %s \n" % e, flush=True)
            return False

def delete_user(username):
    """ 
    Requests the authentication server to delete an user account.

    :param username: User to delete
    :type username: str

    :return: True if success, False if failed
    :rtype: Bool
    """
    token = request.cookies.get('access_token_cookie')

    # Passing token for authenticating user at auth server
    configuration.access_token = token

    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_api.DefaultApi(api_client)
        try:
            api_instance.delete_client(username)
            return True
        except (auth_api.rest.ApiException) as e:
            print("Exception when calling DefaulApi->update_client_info: %s \n" % e, flush=True)
            return False

def get_users():
    """ 
    Requests the authentication server to get all user names.

    :return: List of users if valid, None otherwise
    :rtype: List | None
    """
    token = request.cookies.get('access_token_cookie')

    # Passing token for authenticating user at auth server
    configuration.access_token = token

    with auth_api.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_api.DefaultApi(api_client)
        try:
            users = api_instance.get_clients()[0]
            return users
        except (auth_api.rest.ApiException) as e:
            print("Exception when calling DefaulApi->get_clients: %s \n" % e, flush=True)
            return None