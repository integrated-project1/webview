from flask import request, jsonify, render_template
from functools import wraps
from auth.auth_utils import decode_token

# Different required scopess
EXPORT_SCOPE = "read:data"
REGISTER_SCOPE = "read:clients delete:clients register:clients read:data"
BUILDING_SCOPE = "write:buildings update:buildings delete:buildings write:data"

def scope_checking(required_scope=None, access_token=None):
    """
    Checks if the current scope correspond to the required scope.

    :param required_scope (optional): Required scope to access the route
    :type required_scope: str
    :param access_token: Token provided by the httponly cookie
    :type access_token: str

    :return: True if correct scope, False + Error message if not valid.
    :rtype: [BOOL, ERROR]
    """
    if access_token is None:
        token = request.cookies.get('access_token_cookie')
    else:
        token = access_token
    decoded = decode_token(token)
    if decoded is None:
        return False, render_template("error.html", msg="401 : A valid token is needed !"), 401

    if required_scope is None:
        return True, []

    current_scope = decoded['scope']
    test_scope = all(elem in current_scope.split() for elem in required_scope.split())
    if test_scope:
        return True, []
    return False, render_template("error.html", msg="401 : You do not have access to this Feature!"), 401

def set_scope_permissions(token):
    """
    Sets current user permission to handle view.

    :param access_token: Token provided by the httponly cookie
    :type access_token: str

   :return: dict containing permissions
    :rtype: dict
    """
    Perm = {
        "is_auth" : True
    }
    if scope_checking(required_scope=EXPORT_SCOPE, access_token=token)[0]:
        Perm['can_export'] = True
    else:
        Perm['can_export'] = False
    if scope_checking(required_scope=BUILDING_SCOPE, access_token=token)[0]:
        Perm['can_add_build'] = True
    else:
        Perm['can_add_build'] = False
    if scope_checking(required_scope=REGISTER_SCOPE, access_token=token)[0]:
        Perm['can_register_manage'] = True
    else:
        Perm['can_register_manage'] = False
    return Perm
    


##########################################################
#DECORATORS                                              #
##########################################################

def token_required(fn):
    """
    Allow access if a valid token is used.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        test = scope_checking()
        if test[0]:
            return fn(*args, **kwargs)
        return test[1]
    return wrapper

def export_required(fn):
    """
    Allow access to export data if valid scope.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        test = scope_checking(required_scope=EXPORT_SCOPE)
        if test[0]:
            return fn(*args, **kwargs)
        return test[1]
    return wrapper

def register_required(fn):
    """
    Allow access to register and manage users if a valid scope.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        test = scope_checking(required_scope=REGISTER_SCOPE)
        if test[0]:
            return fn(*args, **kwargs)
        return test[1]
    return wrapper

def building_required(fn):
    """
    Allow access to modify buildings if valid scope.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        test = scope_checking(required_scope=BUILDING_SCOPE)
        if test[0]:
            return fn(*args, **kwargs)
        return test[1]
    return wrapper